<?php get_header();?>
<div class="container bg-light rounded mt-5 mb-5">

	<!-- Title -->
	<h1 class="mt-4 text-secondary"><?php the_title(); ?></h1>

	<!-- Author -->
	<p class="lead text-secondary">
		by
		<a href="#" class="text-secondary"><?php the_author(); ?></a>
	</p>

	<hr>

	<p class="lead text-secondary">
		Categories:
		<a href="#" class="text-secondary">
			<?php
			if (has_term('', 'lesson'))
			{
				foreach (wp_get_post_terms( $post->ID, 'lesson', array('fields' => 'names')) as $term)
				{
					echo $term . "\t";
				}
			}
			?>

		</a>
	</p>

	<hr>

	<!-- Date/Time -->
	<p class="text-secondary"><?php the_modified_time('F j, Y g:i a'); ?></p>

	<hr>

	<!-- Preview Image -->
	<img class="img-fluid rounded" src="<?php the_field('title_image');?>" alt="">

	<hr>

	<!-- Post Content -->
	<p class="lead text-secondary"><?php the_post(); the_content(); ?></p>

	<blockquote class="blockquote">
		<p class="mb-0 text-secondary">Полезные ресурсы:</p>
	</blockquote>

	<p>
		<iframe width="560" height="315"
		        src=" <?php if(the_field('lection_video')){
			        the_field('lection_video');
		        } ?>"
		        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
		        allowfullscreen></iframe>

	</p>

	<p></p>

	<hr>

</div>

<?php get_footer(); ?>