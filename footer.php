

<!-- Footer -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-bottom">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
		<?php
		wp_nav_menu( array(
			'theme_location'  => '',
			'menu'            => 'Footer menu',
			'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
			'container'       => 'div',
			'container_class' => 'collapse navbar-collapse',
			'container_id'    => 'bs-example-navbar-collapse-1',
			'menu_class'      => 'navbar-nav ml-auto',
			'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
			'walker'          => new WP_Bootstrap_Navwalker(),
		) );
		?>
    </div>
</nav>


		<?php
/*		wp_nav_menu( [
			'theme_location'  => '',
			'menu'            => 'Footer menu',
			'container'       => 'ul',
			'container_class' => 'mui-list--inline mui--text-body2',
			'container_id'    => '',
			'menu_class'      => 'mui-list--inline mui--text-body2',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 0,
			'walker'          => '',
		] );
		*/?>

<?php wp_footer(); ?>
</body>
</html>