<?php get_header(); ?>

<!-- Page Content -->
<div class="container">

	<div class="row">
		<!-- Post Content Column -->
		<div class="col-lg-8 mt-5 mb-5">
			<?php
			// параметры по умолчанию
			$posts = get_posts( array(
				'numberposts' => 5,
				'category'    => 0,
				'orderby'     => 'date',
				'order'       => 'DESC',
				'include'     => array(),
				'exclude'     => array(),
				'meta_key'    => '',
				'meta_value'  =>'',
				'category_name' => get_term(),
				'post_type'   => 'lesson',
				'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
			) );

			foreach( $posts as $post ) {
				setup_postdata( $post );
				?>

				<div class="container bg-light rounded">

					<!-- Title -->
					<h3 class="mt-4 text-secondary h1"><?php the_title(); ?></h3>

					<!-- Author -->
					<p class="lead text-secondary">
						by
						<a href="#" class="text-secondary"><?php the_author(); ?></a>
					</p>

					<hr>

					<p class="lead text-secondary">
						Categories:
						<a href="#" class="text-secondary">
							<?php
							if (has_term('', 'lesson'))
							{
								foreach (wp_get_post_terms( $post->ID, 'lesson', array('fields' => 'names')) as $term)
								{
									echo $term . "\t";
								}
							}
							?>

						</a>
					</p>

					<hr>

					<!-- Date/Time -->
					<p class="text-secondary"><?php the_modified_time('F j, Y g:i a'); ?></p>

					<hr>

					<!-- Preview Image -->
					<img class="img-fluid rounded" src="<?php the_field('title_image');?>" alt="">

					<hr>

					<!-- Post Content -->
					<p class="lead text-secondary"><?php the_excerpt(); ?></p>

					<blockquote class="blockquote">
						<p class="mb-0 text-secondary">Полезные ресурсы:</p>
					</blockquote>

					<p>
						<iframe width="560" height="315"
						        src=" <?php if(the_field('lection_video')){
							        the_field('lection_video');
						        } ?>"
						        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
						        allowfullscreen></iframe>

					</p>

					<p></p>

					<hr>

				</div>
				<?php
			}
			wp_reset_postdata(); // сброс
			?>
		</div>


		<!-- Sidebar Widgets Column -->
		<div class="col-md-4 mt-5">


			<!-- Categories Widget -->
			<?php dynamic_sidebar('sidebar-0'); ?>



		</div>

	</div>
	<!-- /.row -->

</div>
<!-- /.container -->


<?php get_footer(); ?>
