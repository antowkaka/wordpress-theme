<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="keywords" content="wordpress, blog, news, blog-wordpress, seo" />
    <meta property="og:title" content="Welcome to my first blog!"/>
    <meta property="og:description" content="My first blog about IT-news and education"/>
    <meta property="og:type" content="article"/>
    <title><?php

	    if ( is_category() ) {
		    echo 'Category Archive for &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
	    } elseif ( is_tag() ) {
		    echo 'Tag Archive for &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
	    } elseif ( is_archive() ) {
		    wp_title(''); echo ' Archive | '; bloginfo( 'name' );
	    } elseif ( is_search() ) {
		    echo 'Search for &quot;'.wp_specialchars($s).'&quot; | '; bloginfo( 'name' );
	    } elseif ( is_home() ) {
		    bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
	    }  elseif ( is_404() ) {
		    echo 'Error 404 Not Found | '; bloginfo( 'name' );
	    } elseif ( is_single() ) {
		    wp_title('');
	    } else {
		    echo the_title();
	    }

	    ?></title>
    <?php wp_head(); ?>
</head>
<body class="bg-dark">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="#"><?php the_custom_logo(); ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
	                <?php
	                wp_nav_menu( array(
		                'theme_location'  => '',
		                'menu'            => 'Header menu',
		                'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
		                'container'       => 'div',
		                'container_class' => 'collapse navbar-collapse',
		                'container_id'    => 'bs-example-navbar-collapse-1',
		                'menu_class'      => 'navbar-nav ml-auto',
		                'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
		                'walker'          => new WP_Bootstrap_Navwalker(),
	                ) );
	                ?>
        </div>
    </nav>


