<?php get_header(); ?>

<!-- Page Content -->
<div class="container">

    <div class="row">
        <!-- Post Content Column -->
        <div class="col-lg-8 mt-5 mb-5">
			<?php
			// параметры по умолчанию
			$posts = get_posts( array(
				'numberposts' => 5,
				'category'    => 0,
				'orderby'     => 'date',
				'order'       => 'DESC',
				'include'     => array(),
				'exclude'     => array(),
				'meta_key'    => '',
				'meta_value'  =>'',
				'post_type'   => 'post',
				'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
			) );

			foreach( $posts as $post ) {
				setup_postdata( $post );
				?>

                <div class="container bg-light rounded">

                    <!-- Title -->
                    <h1 class="mt-4 text-secondary"><?php the_title(); ?></h1>

                    <!-- Author -->
                    <p class="lead text-secondary">
                        by
                        <a href="#" class="text-secondary"><?php the_author(); ?></a>
                    </p>

                    <hr>

                    <p class="lead text-secondary">
                        Categories:
                        <a href="#" class="text-secondary">
							<?php
							if (has_category( '', $post->ID ))
							{
								foreach( get_the_category() as $category )
								{
									echo $category->name . "\t";
								}
							}
							?>

                        </a>
                    </p>

                    <hr>

                    <!-- Date/Time -->
                    <p class="text-secondary"><?php the_modified_time('F j, Y g:i a'); ?></p>

                    <hr>

                    <!-- Preview Image -->
                    <img class="img-fluid rounded" src="<?php the_field('title_image');?>" alt="">

                    <hr>

                    <!-- Post Content -->
                    <p class="lead text-secondary"><?php the_excerpt(); ?></p>

                    <hr>

                </div>
				<?php
			}
			wp_reset_postdata(); // сброс
			?>
        </div>


        <?php get_sidebar('posts'); ?>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->


<?php get_footer(); ?>
