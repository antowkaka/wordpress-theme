<?php
/*
 * Template Name: Contact Page
*/
get_header();
?>
<div class="d-flex form-container align-items-center bg-dark">
	<div class="container col-sm-10 col-md-6 col-lg-4 offset-sm-1 offset-md-3 offset-lg-4">
		<?php echo do_shortcode('[contact-form-7 id="22" title="My form"]')?>
	</div>
</div>


<?php get_footer(); ?>





