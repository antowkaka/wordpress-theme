<?php

add_action('wp_enqueue_scripts', 'mui_style_script');

function mui_style_script()
{
    wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
    wp_enqueue_style('index-style', get_template_directory_uri() . '/assets/css/blog-post.css');

    wp_enqueue_script('bootstrap-script', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js',
                      array(), null, true);

    wp_enqueue_script('readmore', get_template_directory_uri() . '/assets/js/jquery.min.js',
                      array(), null, true);
}

function wpschool_create_posttype() {
    register_post_type( 'lesson',
        array(
            'labels' => array(
                'name' => __( 'Лекции' ),
                'singular_name' => __( 'Лекции' ),
                'menu_name' => __( 'Лекции', 'root' ),
                'all_items' => __( 'Все лекции', 'root' ),
                'view_item' => __( 'Смотреть лекцию', 'root' ),
                'add_new_item' => __( 'Добавить новую лекцию', 'root' ),
                'add_new' => __( 'Добавить лекцию', 'root' ),
                'edit_item' => __( 'Редактировать лекцию', 'root' ),
                'update_item' => __( 'Обновить лекцию', 'root' ),
                'search_items' => __( 'Искать лекцию', 'root' ),
                'not_found' => __( 'Лекию не найдено', 'root' ),
                'not_found_in_trash' => __( 'Лекцию не найдено в корзине', 'root' ),
            ),
            'public' => true,
            'has_archive' => true,
            'description' => __( 'Каталог лекций', 'root' ),
            'rewrite' => array('slug' => 'lesson'),
            'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
            'taxonomies' => array( 'lesson' ),
        )
    );
}
add_action( 'init', 'wpschool_create_posttype' );


function create_taxonomy(){
    // список параметров: wp-kama.ru/function/get_taxonomy_labels
    register_taxonomy( 'lesson', [ 'lesson' ], [
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => [
            'name'              => 'Название лекции',
            'singular_name'     => 'Название лекции',
            'search_items'      => 'Search Genres',
            'all_items'         => 'All Genres',
            'view_item '        => 'View Genre',
            'parent_item'       => 'Parent Genre',
            'parent_item_colon' => 'Parent Genre:',
            'edit_item'         => 'Edit Genre',
            'update_item'       => 'Update Genre',
            'add_new_item'      => 'Add New Genre',
            'new_item_name'     => 'New Genre Name',
            'menu_name'         => 'Название лекции',
        ],
        'description'           => 'These are Deveducation frontend lections', // описание таксономии
        'public'                => true,
        'hierarchical'          => false,
        'rewrite'               => ['slug' => 'lesson'],
        'capabilities'          => array(),
        'meta_box_cb'           => null,
        'show_admin_column'     => false,
        'show_in_rest'          => null, // добавить в REST API
        'rest_base'             => null, // $taxonomy
        // '_builtin'              => false,
        //'update_count_callback' => '_update_post_term_count',
    ] );
}

add_action( 'init', 'create_taxonomy' );

add_theme_support('custom-logo');
add_theme_support('menus');

/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

add_action( 'widgets_init', 'register_my_widgets' );
function register_my_widgets(){
	register_sidebar( array(
		'name'          => sprintf(__('Lections %d'), $i ),
		'id'            => "lections-$i",
		'description'   => '',
		'class'         => 'col-md-4 mt-5',
		'before_widget' => '<div class="card my-4">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<h5 class="card-header">',
		'after_title'   => "</h5>\n",
	) );

	register_sidebar( array(
		'name'          => sprintf(__('Posts %d'), $i ),
		'id'            => "posts-$i",
		'description'   => '',
		'class'         => 'col-md-4 mt-5',
		'before_widget' => '<div class="card my-4">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<a href="#">',
		'after_title'   => "</a\n",
	) );
}

add_filter( 'excerpt_more', 'new_excerpt_more' );
function new_excerpt_more( $more ){
	global $post;
	return '<a href="'. get_permalink($post) . '">Читать дальше...</a>';
}